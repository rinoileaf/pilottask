
🚀 RN Pilot Task
=======================================

[![React Native](https://img.shields.io/badge/React%20Native-v0.55-blue.svg)](https://facebook.github.io/react-native/)
[![React Navigation V2](https://img.shields.io/badge/React%20Navigation-v2..0.1-blue.svg)](https://reactnavigation.org/)


This project is configured with redux, redux saga 
and redux persist. Latest version of react-navigation (v2.0.1) 

## Features

* [Redux](http://redux.js.org/)
* [Redux Saga](https://redux-saga.js.org/)
* [NativeBase](https://nativebase.io/)
* [React Navigation](https://reactnavigation.org/) 

## Prerequisites

* [Node](https://nodejs.org) v8.10 (it is recommended to install it via [NVM](https://github.com/creationix/nvm))
* [Yarn](https://yarnpkg.com/)
* A development machine set up for React Native by following [these instructions](https://facebook.github.io/react-native/docs/getting-started.html)

## Getting Started

1. Clone this repo, `git clone https://github.com/harishileaf/RNTrialTask.git <your project name>`
2. Go to project's root directory, `cd <your project name>`
3. Run `yarn` or `npm install` to install dependencies  

4. Start the packager with `npm start`
5. Connect a mobile device to your development machine
6. Run the test application:
  * On Android:
    * Run `react-native run-android`
  * On iOS:
    * Open `ios/YourReacTproject.xcodeproj` in Xcode
    * Hit `Run` after selecting the desired device
7. Enjoy!!!


