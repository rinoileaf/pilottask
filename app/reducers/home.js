
import {
    API_REQUEST_HOME,
    API_SUCCESS_HOME,
    API_FAILURE_HOME,
    API_ERROR_HOME
} from '../screens/Home/actions';

const initialState = {
    isLoading : true,
    message : '',
    firstName : '',
    user : {},
}

export default (state = initialState , action ) => {

    switch ( action.type ) {    
        
        case API_REQUEST_HOME :
            return {
                ...state,
                isLoading : true,
            }

        case API_SUCCESS_HOME :
            return {
                ...state,
                isLoading : false,
                message : action.result.message,
                user : action.result.data,
            }
        
        default : 
            return state
    }
}