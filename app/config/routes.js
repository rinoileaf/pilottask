import React from 'react';
import { StackNavigator , DrawerNavigator , SwitchNavigator , createStackNavigator} from 'react-navigation';

import Home from '../screens/Home';

export default  createStackNavigator ({

    Home : {
        screen : Home,
    }
    
},{
    initialRouteName : 'Home',
});