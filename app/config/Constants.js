import { Dimensions } from 'react-native';

const Constants = {

    Main : {
        baseUrl : 'http://www.mocky.io/v2/',
    },

    Api : {
        homeApi : '',
    },

    Dimension : {
        ScreenWidth ( percent  = 1 ){
            return Dimensions.get('window').width * percent;
        },

        ScreenHeight ( percent = 1) {
            return Dimensions.get('window').height * percent;
        },
        deviceHeight : Dimensions.get("window").height,
        deviceWidth  : Dimensions.get('window').width,
    } ,


    Color : {
        primary : '#7140FE',
        //basic 
        white : '#fff',

    },

}

export default Constants;