import PropTypes from 'prop-types';
import React, {Component} from 'react';
import { connect } from 'react-redux';
import {
  Platform, 
  StyleSheet, 
  View,
  ScrollView,
} from 'react-native';
import {
  Thumbnail,
  Text,
} from 'native-base';

import Constants from '@config/Constants'
import styles from './styles';

import {
  getInitialHome,
} from './actions';

import Loader from '@components/common/Loader';
import { CustomContainer } from '@components/common/Container';
import { HorizontalSwipeView } from '@components/common/HorizontalSwipeView';
import { HomeParentList } from '../../components/common/HomeParentList';
import { HomeCategoryOne } from '../../components/home/HomeCategoryOne';
import { HomeCategoryTwo } from '../../components/home/HomeCategoryTwo';

const TEMP_IMAGE_URI = 'https://homepages.cae.wisc.edu/~ece533/images/watch.png';
import homeData from './data';

class Home extends Component {

  static navigationOptions = {
    title: 'Home',
    headerStyle: {
      backgroundColor: '#7140FE',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
      justifyContent : 'center',
      alignItems: 'center',
    },
  };


  static propTypes = {
    navigation : PropTypes.object,
    dispatch : PropTypes.func,
    status : PropTypes.number,
    isLoading : PropTypes.bool,
    message : PropTypes.string,
    user : PropTypes.object,
    firstName : PropTypes.string,
  }

  constructor(props){
    super(props);
    console.log("Home Screen");
  }  

  componentDidMount() {
    this.getHomeApi()
  }

  //------------------ Logic ----------------//

  getHomeApi() {
    console.log('fn : Home Api Calling');
    this.props.getInitialHome();
  }


  //--------------------- UI ----------------//

  render() {

    if(this.props.isLoading === true) {
      return (
        <Loader/>
      );  
    }

    return (
      <CustomContainer>
        {this.renderMainScreen()}
      </CustomContainer>
    );
  }

  //{<Text> {this.props.user.avatar} </Text>}
  renderMainScreen() {
    return(
      <ScrollView
          showsVerticalScrollIndicator={false}>

          {this.renderUserHead()}
          {this.renderCardOne('', homeData.homeCategories)}
          {this.renderCardTwo('Here are some Categories You may like to read', homeData.bags)}

      </ScrollView>
    );
  }

  renderUserHead() {
    return(
      <View style = {styles.userDetails}>
        <Thumbnail large 
            style = {styles.thumbNail}
            source={{uri:TEMP_IMAGE_URI}} />
        <Text style = {styles.userName}>{`Hi ${this.props.user.first_name} ,`}</Text>
        <Text style = {{fontSize: 16,}}>{'You have 12 requests to catch up on today'}</Text>
      </View>
    );
  }

  renderCardOne(title, data) {
    return(
      <View style = {styles.userDetails}>
        <HomeParentList
          title = {title}>
          <HorizontalSwipeView >
          {
              data.items.map((item , idx) => { 
                  return (
                    <HomeCategoryOne
                       key = {idx}/>)
              })
          }
          </HorizontalSwipeView>
        </HomeParentList>
      </View>
    );
  }

  renderCardTwo(title, data) {
    return(
      <View style = {styles.userDetails}>
        <HomeParentList
          title = {title}>
          <HorizontalSwipeView >
          {
              data.items.map((item , idx) => { 
                  return (
                    <HomeCategoryTwo
                      imageUrl = {item.imageUri}
                       key = {idx}/>)
              })
          }
          </HorizontalSwipeView>
        </HomeParentList>
      </View>
    );
  }
}



const mapDispatchToProps = ( dispatch ) => {
  return {
    getInitialHome : () => { dispatch(getInitialHome()) },
  };
};

const mapStateToProps = ( state ) => {
  const isLoading = state.home.isLoading;
  const message =  state.home.message;
  const user = state.home.user;

  return {
    isLoading,
    message,
    user,
   
  }
}


export default connect ( mapStateToProps , mapDispatchToProps ) ( Home );