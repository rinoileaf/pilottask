import {Platform, StyleSheet,} from 'react-native';

import Constants from '@config/Constants'

export default StyleSheet.create({

      userDetails : {
        marginLeft: 20,
        marginTop :  10,
        height : undefined,
      },

      userName : {
        color : Constants.Color.primary,
        fontSize: 35,
        fontWeight: 'bold',
        marginTop: 20,
        height : undefined
      },

      thumbNail : {
        marginTop: 20,
      }
});