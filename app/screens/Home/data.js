'use-strict'
const homeData = {

    homeCategories : {
        items : [
            { 
                name: 'Watches ',
                imageUri: 'https://s-media-cache-ak0.pinimg.com/564x/75/4e/47/754e472b37cc1d8864ecd5ed67e1aa92.jpg',
                desc : 'Test desc',
            }, 
            { 
                name: 'Men Fashion', 
                imageUri: 'https://c1.staticflickr.com/3/2272/2102183091_fb38bb3822.jpg',
                desc : 'Test desc',
            },
            { 
                name: 'Electronics', 
                imageUri: 'https://c1.staticflickr.com/3/2272/2102183091_fb38bb3822.jpg',
                desc : 'Test desc',
            },
            { 
                name: 'Women Fashion', 
                imageUri: 'https://c2.staticflickr.com/4/3311/3670965640_da4a67be4f_z.jpg?zz=1',
                desc : 'Test desc',
            },
            { 
                name: 'Kitchen Items', 
                imageUri: 'https://s-media-cache-ak0.pinimg.com/564x/78/f7/17/78f71796c66a31fbd136efb784a9632d.jpg',
                desc : 'Test desc',
            },
            { 
                name: 'Baby Items', 
                imageUri: 'https://s-media-cache-ak0.pinimg.com/564x/75/4e/47/754e472b37cc1d8864ecd5ed67e1aa92.jpg',
                desc : 'Test desc',
            },
        ]
    },
    bags: {
        title: 'Bags',
        description: 'most popular',
        items: [
            {
                id: 1,
                name: 'Hummingbird Watch strap watch',
                imageUri: 'https://c1.staticflickr.com/3/2337/1637193093_f5661532b0_z.jpg?zz=1',
                prize: 128
            },
            {
                id: 2,
                name: 'metro - chalkboard leather strap watch, 34mm',
                imageUri: 'https://c1.staticflickr.com/3/2272/2102183091_fb38bb3822.jpg',
                prize: 195
            },
            {
                id: 3,
                name: 'Olivia Burton Exclusive Floral Big Dial Watch',
                imageUri: 'https://c1.staticflickr.com/7/6109/6311668225_673b9e1e3c_z.jpg',
                prize: 96
            },
            {
                id: 4,
                name: 'Olivia Burton Enchanted Garden Gray Patent Big Dial Watch',
                imageUri: 'https://c2.staticflickr.com/4/3311/3670965640_da4a67be4f_z.jpg?zz=1',
                prize: 96
            },
        ]
    },

}

export default homeData;