export const API_REQUEST_HOME = 'API_REQUEST_HOME';
export const API_SUCCESS_HOME = 'API_SUCCESS_HOME';
export const API_FAILURE_HOME = 'API_FAILURE_HOME';
export const API_ERROR_HOME = 'API_ERROR_HOME';

export const getInitialHome = () => ({
    type : API_REQUEST_HOME,
});

