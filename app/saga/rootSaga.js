import { fork } from 'redux-saga/effects';

import { 
    apiHomeInitial,
} from './HomeSagas';


export default function* rootSaga() {
    yield [
        fork(apiHomeInitial)
    ];
}