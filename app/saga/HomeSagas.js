import { put, takeLatest , takeEvery, call ,select} from 'redux-saga/effects';

import {
    API_REQUEST_HOME,
    API_SUCCESS_HOME,
    API_FAILURE_HOME,
    API_ERROR_HOME
} from '../screens/Home/actions';

import { Api } from './api/homeApi';

const getHomeApi = function* (action) {
    try {
        console.log('Calling Home Api from saga');
        const response  = yield call(Api.homeApi);
        const result = yield response.json();
        console.log('Home Api result', result);

        if(response.ok) {
            console.log('success action : ')
            yield put({type : API_SUCCESS_HOME , result});
        }

    } catch (error){
        console.log('error',error);
        yield put({type : API_ERROR_HOME})
    }
}

export function* apiHomeInitial() { 
    yield takeEvery(API_REQUEST_HOME , getHomeApi);
}