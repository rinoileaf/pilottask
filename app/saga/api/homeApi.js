import Constants from '../../config/Constants';

const homeApiUrl = Constants.Main.baseUrl + '5b9751e5300000332a0bd52d';

const homeApi = () => fetch( homeApiUrl , {
    method : 'GET',
    headers : {
        'Accept': 'application/json',
    }
})

export const Api = {
    homeApi,
}