import React from 'react';
import {
    View,
    ActivityIndicator,
} from 'react-native';

import Constants from '../../config/Constants';

const Loader = () => {

    return (
        <View style = {{flex: 1,
            alignItems: 'center',
            backgroundColor : Constants.Color.white,
            flexDirection: 'column',
            justifyContent: 'space-around',}}>
              <ActivityIndicator size="large" color= {Constants.Color.primary}/> 
          </View>
    );

}

export default Loader;