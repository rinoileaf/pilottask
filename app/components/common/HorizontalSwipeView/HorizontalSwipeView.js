import PropTypes from 'prop-types';
import React from 'react';
import { ScrollView } from 'react-native';


const HorizontalSwipeView = ({ children }) => {
    return ( 
        <ScrollView 
            contentContainerStyle = {{paddingLeft : 15}}
            horizontal = {true}
            showsHorizontalScrollIndicator={false}
            scrollEventThrottle={200}>

            {children}
        </ScrollView>
    );
}

HorizontalSwipeView.propTypes = {
    children : PropTypes.any,
};

export default HorizontalSwipeView;
