import PropTypes from 'prop-types';
import React from 'react';
import { View , Text  } from 'react-native';

import styles from './styles';

const HomeParentList = ({children , title  }) => {


    return (
        <View>
            <View style = {styles.holder}>

                <Text>{title}</Text>

                { children }

            </View>
        </View>
    );

}

export default HomeParentList;