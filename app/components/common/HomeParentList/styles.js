import { StyleSheet } from 'react-native';

import Constants from '@config/Constants';

export default StyleSheet.create({
    holder : {
        flexDirection : 'column',
        paddingBottom : 20,
        marginTop: 10,
    },
    txtTitle : {
        marginBottom : 10,
    },
    
});