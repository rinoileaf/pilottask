import { StyleSheet } from 'react-native';

import Constants from '../../../config/Constants';

export default StyleSheet.create({

    contianer : {
        flex : 1,
        paddingLeft : 20,
        paddingRight : 20,
        backgroundColor : Constants.Color.white,
        height : Constants.Dimension.ScreenHeight(1), 
    }
});