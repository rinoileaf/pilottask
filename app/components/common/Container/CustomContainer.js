import PropTypes from 'prop-types';
import React from 'react';
import { 
    StatusBar ,
    SafeAreaView,
} from 'react-native';
import {
    Container,
    Text,
} from 'native-base';

import styles from './styles';

const CustomContainer = ({ children }) => (
    <Container style={styles.containerNew}>
        <StatusBar backgroundColor="transparent" barStyle="dark-content" />
        <SafeAreaView>
            {children}
        </SafeAreaView>
    </Container>

);


CustomContainer.propTypes = {
    children: PropTypes.any,
  };
  
export default CustomContainer;