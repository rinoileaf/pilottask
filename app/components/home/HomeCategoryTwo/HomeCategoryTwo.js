import PropTypes from 'prop-types';
import React from 'react';
import { 
    View , 
    Image ,
    Text , 
    TouchableOpacity ,
    } from 'react-native';

import {
    Card
} from 'native-base';

import styles from './styles';

const HomeCategoryTwo = ({onPress ,imageUri }) => {
    return (
        <Card 
            style = {styles.holder}
            onPress = {onPress}>
            <View style = {{paddingLeft : 5, marginTop : 20}}>
                <Image 
                style = {styles.productImage}
                source = {{ uri :  'https://homepages.cae.wisc.edu/~ece533/images/watch.png'}}/>
                <Text style = {{ marginTop : 10, fontSize : 14, fontWeight : 'bold'}}>{"This is a Card"}</Text>
                <Text style = {{ marginTop : 5, fontSize : 10, marginBottom : 10}}>{"Subtile Here"}</Text>
            </View>
           
        </Card>
        );
}

HomeCategoryTwo.propTypes = {
    onPress : PropTypes.func,
    imageUri : PropTypes.string,
};

export default HomeCategoryTwo;