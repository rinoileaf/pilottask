import PropTypes from 'prop-types';
import React from 'react';
import { 
    View , 
    Image ,
    Text , 
    TouchableOpacity ,
    } from 'react-native';

import {
    Card
} from 'native-base';

import styles from './styles';

const HomeCategoryOne = ({onPress , }) => {
    return (
        <Card 
            style = {styles.holder}
            onPress = {onPress}>
            <View style = {{paddingLeft : 5, marginTop : 20}}>
                <Text style = {{ marginTop : 5, fontSize : 14, fontWeight : 'bold'}}>{"This is a Card"}</Text>
                <Text style = {{ marginTop : 5, fontSize : 10}}>{"Subtile Here"}</Text>

                <View style = {{marginTop : 20, flexDirection : 'row', marginBottom: 10,}}>
                    <Text style = {{ backgroundColor : 'blue', color : '#fff', padding : 5, paddingLeft : 5, paddingRight : 5, borderRadius : 4, margin : 4}}>{"Category"}</Text>
                    <Text style = {{ backgroundColor : 'blue', color : '#fff', padding : 5, paddingLeft : 5, paddingRight : 5, borderRadius : 4, margin : 4}}>{"Tag"}</Text>
                </View>
            </View>
           
        </Card>
        );
}

HomeCategoryOne.propTypes = {
    onPress : PropTypes.func,
};

export default HomeCategoryOne;