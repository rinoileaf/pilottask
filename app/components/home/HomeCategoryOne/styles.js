import { StyleSheet , Dimensions } from 'react-native';

import Constants from '@config/Constants';

const { width } = Dimensions.get('window');
const prdWidth = ( width - 45 ) / 2.5 ;

export default StyleSheet.create ({
    holder : {
        width : prdWidth,
        marginRight :  15,
        flexDirection : 'column',
    },
})