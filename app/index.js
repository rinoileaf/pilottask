import React from 'react';
import { Provider } from 'react-redux';

import Navigation from './config/routes';
import store from './config/store';
import Home from './screens/Home';

export default () => {

    return(
        <Provider store = {store}>
            <Navigation/>
        </Provider>
    );
   
}


// export default Home;